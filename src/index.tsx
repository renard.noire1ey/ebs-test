import React, { FunctionComponent, useContext, useEffect, useMemo, useState } from 'react';
import { TranslateContextType, WithTranslationProps } from './types';

const TranslateContext = React.createContext<TranslateContextType>({
  translate: {},
  locale: ''
});

export const useIntl = () => {
  const { locale: language, translate } = useContext(TranslateContext);
  return {
    t: (key: string, params: Record<string, string>): string => {
      // if we have this key
      if (translate[key] && translate[key][language]) {
        const translation = translate[key][language];
        return !params ? translation : Object.keys(params)
          .reduce((acc, currentKey) =>
            acc.replaceAll(`{${currentKey}}`, params[currentKey],
            ), translation);
      }
      return key;
    },
    language
  };
};


export const WithTranslation: FunctionComponent<WithTranslationProps> = ({ translate, locale, children }) => {
  const [translationState, setTranslationState] = useState({
    locale,
    translate,
  });

  /**
   * Leave only valid translations
   */
  const filteredTranslate = useMemo(() => {
    return Object.keys(translate).reduce((acc, currentKey) => {
      // if every key has translation in every languages
      if (
        translate[currentKey] &&
        Object.values(translate[currentKey]).every(Boolean) // not empty
      ) {
        return {
          ...acc,
          [currentKey]: translate[currentKey],
        };
      }
      console.warn(`Translate ${currentKey} no has translates for each language`);
      return acc;
    }, {});
  }, [translate]);

  useEffect(() => {
    setTranslationState({
      locale,
      translate: filteredTranslate,
    });
  }, [filteredTranslate, locale]);


  return (
    <TranslateContext.Provider value={translationState}>
      {children}
    </TranslateContext.Provider>
  );
};

WithTranslation.displayName = 'WithTranslation';
