export type Locale = string;

export interface Translate {
  [translationKey: string]: {
    [language: string]: string
  }
}

export interface TranslateUtils {
  lang: Locale
  t: (key: string, params?: { [key: string]: string }) => string
}

export interface WithTranslationProps {
  translate: Translate;
  locale: Locale;
}

export interface TranslateContextType {
  translate: Translate;
  locale: Locale
}