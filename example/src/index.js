import React, { useState } from 'react';
import { render } from 'react-dom';
import { useIntl, WithTranslation } from 'ebs-intl';

const translateMap = {
    ['Привет друг'] : {
      en: 'Hi friend',
      ro: 'Hi cumatru',
      ru: 'Привет друг',
    },
    ['Василий']: {
      en: 'Vasily',
      ro: 'Vasilii',
      ru: 'Василий',
    },
    ['Привет, {name}. Как ты спал, {name}?']: {
      en: 'Hello, {name}. How did you sleep, {name}?',
      ro: 'Buna, cumatru {name}. Cum ai dormit, {name}?',
      ru: 'Привет, {name}. Как ты спал, {name}?',
  }
};

const TranslatablePage = ({ changeLocale }) => {
  const { t, locale } = useIntl();

  return (
    <>
      <select onChange={changeLocale} value={locale}>
        {Object.keys(translateMap['Василий']).map(locale => <option key={locale} value={locale} >{locale}</option>)}
      </select>
      <div>{t('Привет друг')}</div>
      <div>{t('Привет, {name}. Как ты спал, {name}?', { name: t('Василий') })}</div>
    </>
  )
}

const App = () => {
  const [locale, setLocale] = useState('en')

  const onLocaleChange = (e) => {
    setLocale(e.target.value)
  }

  return (
    <WithTranslation translate={translateMap} locale={locale}>
     <TranslatablePage changeLocale={onLocaleChange}/>
    </WithTranslation>
  )
};

render(<App />, document.getElementById('root'));
